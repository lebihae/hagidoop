.PHONY: all clean

uniq = $(if $1,$(firstword $1) $(call uniq,$(filter-out $(firstword $1),$1)))

# SOURCES_DIR = src src/hdfs src/hdfs/client src/hdfs/serveur src/hdfs/messages src/hdfs/utilitaires src/daemon
# SOURCES = $(wildcard $(patsubst %, %/*.java, $(SOURCES_DIR)))

SOURCES = $(shell find "src" -type f -name "*.java")
SOURCES_DIR = src $(patsubst %/,%,$(call uniq, $(dir $(SOURCES))))

FLAGS = -cp $(shell echo $(SOURCES_DIR) | tr ' ' :)

BINARIES = $(patsubst %.java,%.class,$(SOURCES))
NOM_CLASSES = $(shell echo $(patsubst src/%.java,%,$(SOURCES)) | tr / .)

all: $(BINARIES)

clean:
	rm $$(find "src" -type f -name "*.class")

$(BINARIES): %.class : %.java
	javac $(FLAGS) $<

#executer une classe
$(NOM_CLASSES): $(BINARIES)
	java -Xmx4096m $(FLAGS) $@ $(args)
