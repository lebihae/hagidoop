package daemon;

import java.io.FileReader;
import java.io.IOException;

import interfaces.FileReaderWriter;
import interfaces.MapReduce;
import interfaces.Reader;
import io.ReaderKV;
import io.ReaderText;

public class JobLauncher {

	public static void startJob(MapReduce mr, int format, String fname) throws IOException {
		Reader reader;
		FileReader file = new FileReader(fname);
		try {

			switch (format) {
				case FileReaderWriter.FMT_KV:
					reader = new ReaderKV(file);
					break;

				case FileReaderWriter.FMT_TXT:
					reader = new ReaderText(file);
					break;

				default:
					throw new IllegalArgumentException("Unknown format " + format);
			}
		} finally {
			file.close();
		}
		mr.map(reader, /* todo: writer */ null);
	}
}
