#!/usr/bin/env bash
make
make hdfs.serveur.HdfsServeur &
make hdfs.client.HdfsClient args="test.txt"
kill $(echo $(ps | grep java) | cut -d' ' -f1)
