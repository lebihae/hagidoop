import java.rmi.*;

public class Joueur {

    public static void main(String[] args) {

        try {
            DeItf de = (DeItf)Naming.lookup("//localhost:4000/michel");
            int nb1 = de.getRandom();
            int nb2 = de.getRandom();
            int nb3 = de.getRandom();
            int nbCalls = de.getNbCalls();
            System.out.println("Vous avez tiré " + nb1 + ", " + nb2 + ", " + nb3 + ". Le nombre de tirages total est actuellement " + nbCalls);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
