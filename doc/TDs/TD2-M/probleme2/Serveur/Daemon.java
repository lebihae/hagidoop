import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;

public class Daemon extends UnicastRemoteObject implements DaemonItf {

    public Daemon() throws RemoteException {
    }

    private void localExec(String cmd) {
        System.out.println("Execution de la commande " + cmd);
    }

    public void invoke(String cmd) throws RemoteException {
        localExec(cmd);
    }

    public static void main(String[] args) {
        try {
            Registry registry = LocateRegistry.createRegistry(4000);
            Daemon lucifer = new Daemon();
            Naming.rebind("//localhost:4000/Daemon", lucifer);
            System.out.println("Lucifer publié sur le registre");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
