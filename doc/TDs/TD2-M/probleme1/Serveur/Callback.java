import java.rmi.*;
public interface Callback extends Remote {
    public void onMessage(Message message) throws RemoteException;
}
