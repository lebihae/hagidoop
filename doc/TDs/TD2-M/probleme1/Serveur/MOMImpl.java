import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.util.*;

public class MOMImpl extends UnicastRemoteObject implements MOMItf {

    private HashMap<String, Vector<Callback>> subscribers;

    public MOMImpl() throws RemoteException {
        subscribers = new HashMap<String, Vector<Callback>>();
    }

    public void publish(String topic, Message message) throws RemoteException {
        Vector<Callback> vect = subscribers.get(topic);
        for (int i = 0; i < vect.size(); i++) {
            vect.get(i).onMessage(message);
        }

    }

    public void subscribe(String topic, Callback cb) throws RemoteException {
        subscribers.get(topic).add(cb);
    }

    public static void main(String[] args) {
        try {
            MOMImpl mom = new MOMImpl();
            for (int i = 0; i < args.length; i++) {
                mom.subscribers.put(args[i], new Vector<Callback>());
                System.out.println("Ajout du topic " + args[i]);
            }
            Registry registry = LocateRegistry.createRegistry(4000);
            Naming.rebind("//localhost:4000/maman", mom);
            System.out.println("Maman a été publiée");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
