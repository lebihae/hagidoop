import java.rmi.*;

public class Publisher {

    public static void main(String[] args) {

        try {
            MOMItf mom = (MOMItf)Naming.lookup("//localhost:4000/maman");
            String topic = args[0];
            Message msg = new Message(args[1]);
            mom.publish(topic, msg);
            System.out.println("Message " + msg.text + " publié sur le topic " + topic);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
