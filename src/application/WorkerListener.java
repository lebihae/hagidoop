package application;

import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.util.concurrent.Semaphore;

import interfaces.FileReaderWriter;
import interfaces.KV;
import interfaces.Map;
import io.NetworkReaderWriterKV;
import io.QueueResults;
import daemon.Worker;

/** Un thread chargé de faire le lien entre le client
 * et un Worker */
public class WorkerListener implements Runnable {

  /** La fifo à laquelle envoyer tous les KVs */
  private QueueResults fifo;

  /** Le sémaphore pour indiquer le nombre de taches terminées */
  private Semaphore tasksEnded;

  /** L'adresse du worker */
  private String hostname;

  /** Le port pour exécuter le map en RMI */
  private int portRMI;

  /** Le port pour recevoir les résultats via une socket */
  private int portSocket;

  /** L'opération map a exécuter sur le serveur */
  private Map map;

  /** Le reader pour lire le fichier stocké sur le serveur */
  private FileReaderWriter reader;

  /** Créer un nouvel objet à partir des informations sur le worker distant
   * et de l'opération de map
   * @param fifo la fifo qui doit recevoir tous les KVs
   * @param hostname adresse du worker
   * @param portRMI le port pour le RMI
   * @param portSocket le port pour ouvrir des Sockets 
   * @param map l'opération map a effectuer sur le worker
   * @param reader le lecteur du fichier pour le worker
   */
  public WorkerListener(QueueResults fifo, Semaphore tasksEnded, String hostname, int portRMI,
      int portSocket, Map map, FileReaderWriter reader) {
    this.fifo = fifo;
    this.tasksEnded = tasksEnded;
    this.hostname = hostname;
    this.portRMI = portRMI;
    this.portSocket = portSocket;
    this.map = map;
    this.reader = reader;
  }

  @Override
  /** Lancer le map sur le worker et
   * envoyer les résultats à la QueueResults */
  public void run() {
    try {
      NetworkReaderWriterKV connexion = new NetworkReaderWriterKV(hostname, portSocket);
      Worker worker = (Worker) Naming.lookup("//" + hostname + ":" + portRMI + "/worker");
      System.out.println("map debut");
      worker.runMap(map, reader, connexion);
      System.out.println("map fin");

      KV nextKV = connexion.read();
      while (nextKV != null) {
        fifo.write(nextKV);
        nextKV = connexion.read();
      }
      System.out.println("reception fin");
    } catch (IOException e) {
      System.out.println("ERREUR AVEC LE WORKER : " + hostname);
      e.printStackTrace();
    } catch (NotBoundException e) {
      System.out.println("ERREUR AVEC LE WORKER : " + hostname);
      e.printStackTrace();
    } catch (Exception e) {
      System.out.println("ERREUR AVEC LE WORKER : " + hostname);
      throw e;
    }
    finally {
      /* On prévient qu'on a fini le thread */
      tasksEnded.release();
    }
    System.out.println("thread fin");
  }

}
