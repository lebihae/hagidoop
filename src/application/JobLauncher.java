package application;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;

import config.Project;
import config.WorkerInfo;
import hdfs.client.HdfsClient;
import interfaces.Writer;
import interfaces.FileReaderWriter;
import interfaces.MapReduce;
import interfaces.Reduce;
import io.QueueResults;
import io.ReaderText;

public class JobLauncher {

  // à faire dans l'ordre:
  // 1. récupérer la liste des serveurs
  // 2. créer un thread de Job pour chaque serveur qui execute:
  //      - lancer le job sur le serveur
  //      - ouvrir une socket pour recevoir les résultats du serveur
  //      - ajoute les KVs obtenus à la file "atomic"
  //      - si la socket se ferme avant de recevoir caractère de fin (4 fois le séparateur)
  //        alors on affiche une erreur à l'écran (dans le futur on stoppera le JobLauncher)
  // 3. lancer le réduceur qui execute:
  //      - tantQue tous les threads sont pas finis (utiliser un semaphore ou une barrière)
  //          lire les KVs dans la file atomique (= reçus de n'importe quel thread) et les traiter avec le réduceur
  //      - écrire le résultat dans un fichier

  /** Un wrapper tout simple d'un Reduce pour le lancer dans un nouveau thread */
  private static class ThreadReduce implements Runnable {

    /** Le code a exécuter dans le thread */
    private Reduce reducer;

    /** la fifo pour lire les KVs */
    private QueueResults fifo;

    /** le code pour écrire dans un fichier */
    private FileReaderWriter writer;

    /** Créer le Runnable
     * @param reducer le code a exécuter
     * @param fifo la fifo pour lire les KVs
     * @param writer le code pour écrire le résultat dans un fichier
     */
    public ThreadReduce(Reduce reducer, QueueResults fifo, FileReaderWriter writer) {
      this.reducer = reducer;
      this.fifo = fifo;
      this.writer = writer;
    }

    @Override
    public void run() {
      /* Ouvrir le fichier en écriture */
      writer.open("w");

      /* Lancer le reduceur */
      reducer.reduce(fifo, writer);

      /* Fermer le fichier */
      writer.close();
    }
  }

  public static void startJob(MapReduce mr, int format, String fname) {
    // Reader reader;
    // FileReader file = new FileReader(fname);
    // try {

    // 	switch (format) {
    // 		case FileReaderWriter.FMT_KV:
    // 			reader = new ReaderKV(file);
    // 			break;

    // 		case FileReaderWriter.FMT_TXT:
    // 			reader = new ReaderText(file);
    // 			break;

    // 		default:
    // 			throw new IllegalArgumentException("Unknown format " + format);
    // 	}
    // } finally {
    // 	file.close();
    // }
    // mr.map(reader, /* todo: writer */ null);

    QueueResults fifo = new QueueResults();

    /* Lancer le Reducer */
    Thread reducer = new Thread(new ThreadReduce(mr, fifo, new ReaderText("count-res_mapReduce"))); // TODO changer par un vrai writer
    reducer.start();

    /* On récupère la liste des fragments */
    ConcurrentMap<Integer, WorkerInfo> listeFragments = HdfsClient.getHostsFragments(fname);

    /* On récupère le numéro de fragment max (== nombre de fragments - 1) */
    int nbFrags = 0; /* le nombre de fragments */
    for (int idReceveur : listeFragments.keySet())
      nbFrags = Integer.max(nbFrags, idReceveur + 1);

    /* Compter le nombres de threads qu'on lance
     * pour pouvoir attendre qu'ils aient tous fini */
    Semaphore tasksEnded = new Semaphore(0);
    int nbLances;

    /* Lancer le map sur tous les fragments */
    for (nbLances = 0; nbLances < nbFrags; nbLances++) {
      WorkerInfo w = listeFragments.get(nbLances);

      if (w == null) {
        System.out.println("ERREUR: Le fragment numéro " + nbLances + " n'existe sur aucun serveur, abandon");
        System.exit(1);
      }

      WorkerListener worker = new WorkerListener(
          fifo,
          tasksEnded,
          w.hostname,
          w.portRMI,
          w.portSocket,
          mr,
          new ReaderText(fname + "_part" + nbLances)
          );

      (new Thread(worker)).start();
    }

    /* On attend la fin de tous les threads */
    while (nbLances > 0) {
      try {
        tasksEnded.acquire();
        nbLances--;
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    /* On indique au réduceur que c'est fini */
    fifo.write(null);

    /* On attent que le réduceur aie fini avant de quitter */
    try {
      reducer.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
