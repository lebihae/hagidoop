package hdfs.serveur;

import java.net.*;
import java.io.*;

import hdfs.messages.*;

public class HdfsServeur {

  public static void main(String[] args) {

    if (args.length != 2) {
      System.out.println("usage: HdfsServeur <port> <path>");
      return;
    }

    ServerSocket ssock;
    int port = Integer.parseInt(args[0]);
    String chemin = args[1] + "/";

    try {
      ssock = new ServerSocket(port);
      System.out.println("Serveur prêt");

      while (true) {
        Socket s = ssock.accept();
        System.out.println("Connexion d'un client");
        InputStream is = s.getInputStream();
        ObjectInputStream ois = new ObjectInputStream(is);
        HdfsRequete req = (HdfsRequete) ois.readObject();
        // System.out.println("Requête reçue " + req);
        HdfsStatus status = req.executer(chemin);
        try {
          status.verifierOK();
        } catch (Exception e) {
          e.printStackTrace();
        }
        System.out.println("Result = " + status);

        OutputStream os = s.getOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(os);
        oos.writeObject(status);
        System.out.println("Réponse envoyée");

        oos.close();
        os.close();
        ois.close();
        is.close();
        s.close();

        /* IMPORTANT: on indique au garbage collector qu'on veut qu'il
         * nettoie la ram car on a potentiellement envoyé des objets très lourds */
        System.gc();
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
