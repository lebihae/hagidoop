package hdfs.client;

import java.net.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.io.*;

import hdfs.messages.*;

/** Un sender Hdfs qui envoie une partie du fichier au serveur
 * qui lui a été attribué. */
public class HdfsSuppresseur implements Runnable {

  /** Limiter le nombre de threads actuellement lancés
   * (surtout pour éviter une trop grosse utilisation de RAM) */
  private Semaphore limitThreads;

  /** Si on rencontre une erreur, on met problemeRencontre a true
   * pour indiquer qu'il y a eu un soucis */
  private AtomicBoolean problemeRencontre;

  /** La socket pour l'envoi et la réception d'objets */
  private Socket sock;

  /** Le chemin du fichier d'entrée sur le client */
  private String cheminFichier;

  /** Le contenu à écrire dans le fichier sur le serveur */
  private String contenu;

  /** Nom de la machine avec le serveur hdfs */
  private String hostname;

  /** Port de la machine avec le serveur hdfs */
  private int port;

  /** Créer l'envoyeur (pensé pour être dans un Thread)
   * à partir de toutes les infos nécessaires */
  public HdfsSuppresseur(Semaphore limitThreads, AtomicBoolean problemeRencontre,
      String hostname, int port,
      String cheminFichier) {
    this.limitThreads = limitThreads;
    this.problemeRencontre = problemeRencontre;
    this.hostname = hostname;
    this.port = port;
    this.cheminFichier = cheminFichier;
  }

  @Override
  public void run() {
    try {
      this.sock = new Socket(hostname, port);
      String chemin_split[] = cheminFichier.split("/");
      String nomFichier =
        cheminFichier.split("/")[chemin_split.length - 1];

      OutputStream os = sock.getOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(os);
      oos.writeObject(new HdfsRequeteSuppression(nomFichier));

      InputStream is = sock.getInputStream();
      ObjectInputStream ois = new ObjectInputStream(is);
      ((HdfsStatus) ois.readObject()).verifierOK();
      System.out.println(nomFichier + " supprimé avec succès sur " + hostname);

      ois.close();
      oos.close();
      is.close();
      os.close();
      sock.close();
    }
    catch(Exception e) {
      e.printStackTrace();
      problemeRencontre.set(true);
      System.out.println("ERREUR AVEC LE SERVEUR : " + hostname);
    }
    limitThreads.release();
  }

}
