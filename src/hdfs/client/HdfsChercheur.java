package hdfs.client;

import java.net.*;
import java.io.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.*;

import config.WorkerInfo;

import java.util.concurrent.ConcurrentMap;

import hdfs.messages.*;

/** Demander au serveur quels sont les fragments du Fichier
 * qu'il possède et remplir les informations pour pouvoir ensuite
 * lire et reconstruire le fichier avec HdfsReceveur */
public class HdfsChercheur implements Runnable {

  /** Limiter le nombre de threads actuellement lancés
   * (surtout pour éviter une trop grosse utilisation de RAM) */
  private Semaphore limitThreads;

  /** Si on rencontre une erreur, on met problemeRencontre a true
   * pour indiquer qu'il y a eu un soucis */
  private AtomicBoolean problemeRencontre;

  /** La socket pour l'envoi et la réception d'objets */
  private Socket sock;

  /** Le chemin du fichier d'entrée sur le client */
  private String cheminFichier;

  /** Infos sur le serveur à contacter */
  private WorkerInfo host;

  /** Passer le résultat au thread principal (id_fragment, worker_qui_stocke) */
  private ConcurrentMap<Integer, WorkerInfo> resultats;

  /** Créer le receveur (pensé pour être dans un Thread)
   * à partir de toutes les infos nécessaires */
  public HdfsChercheur(Semaphore limitThreads, AtomicBoolean problemeRencontre,
      WorkerInfo host,
      String cheminFichier, ConcurrentMap<Integer, WorkerInfo> resultats) {
    this.limitThreads = limitThreads;
    this.problemeRencontre = problemeRencontre;
    this.host = host;
    this.cheminFichier = cheminFichier;
    this.resultats = resultats;
  }

  @Override
  public void run() {
    try {
      this.sock = new Socket(host.hostname, host.portHdfs);
      String chemin_split[] = cheminFichier.split("/");
      String nomFichier =
        cheminFichier.split("/")[chemin_split.length - 1];

      OutputStream os = sock.getOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(os);
      oos.writeObject(new HdfsRequeteLister(nomFichier));

      InputStream is = sock.getInputStream();
      ObjectInputStream ois = new ObjectInputStream(is);
      HdfsStatus status = (HdfsStatus) ois.readObject();
      status.verifierOK();
      // System.out.println("liste des fragments sur " + hostname
      //     + ": " + status.getReponse());

      if (status.getReponse().length() > 0) {
        for (String frag : status.getReponse().split("/")) {
          int n = Integer.parseInt(frag.split("_part")[1]);
          resultats.put(n, host);
        }
      }

      ois.close();
      oos.close();
      is.close();
      os.close();
      sock.close();
    }
    catch(Exception e) {
      System.out.println("ERREUR RENCONTREE AVEC LE SERVEUR : " + host.hostname);
      e.printStackTrace();
      problemeRencontre.set(true);
    }
    limitThreads.release();
  }

}
