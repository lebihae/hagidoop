package hdfs.client;

import hdfs.messages.*;
import interfaces.Map;

import java.awt.desktop.PrintFilesEvent;
import java.io.*;
import java.net.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.*;
import java.util.List;

import config.Project;
import config.WorkerInfo;;

public class HdfsClient {

  final private static int NB_THREADS = 10;

  private static void usage() {
    System.out.println("Usage: java HdfsClient read <file>");
    System.out.println("Usage: java HdfsClient write <txt|kv> <file>");
    System.out.println("Usage: java HdfsClient delete <file>");
  }

  public static void HdfsDelete(String fname) {
    AtomicBoolean problemeRencontre = new AtomicBoolean(false);

    System.out.println("Suppression du fichier sur tous les serveurs");

    Semaphore limitThreads = new Semaphore(NB_THREADS);

    /* hack sale, utiliser une pool de threads */
    try {
      for (WorkerInfo host : Project.getWorkers()) {
        new Thread(new HdfsSuppresseur(limitThreads, problemeRencontre,
              host.hostname, host.portHdfs, fname)).start();
        limitThreads.acquire();
      }

      for (int i = 0; i < NB_THREADS; i++)
        limitThreads.acquire();
    } catch (Exception e) { e.printStackTrace(); }

    if (problemeRencontre.get())
      throw new RuntimeException("Une erreur a été rencontré lors de la suppression sur un des serveurs, abandon");
  }

  public static void HdfsWrite(int fmt, String fname) {
    Semaphore limitThreads = new Semaphore(NB_THREADS);

    AtomicBoolean problemeRencontre = new AtomicBoolean(false);

    try {
      File fichier = new File(fname);
      BufferedReader br = new BufferedReader(new FileReader(fichier));
      boolean eof = false;
      int id = 0;

      StringBuilder sb = new StringBuilder();
      StringBuilder finLigne = new StringBuilder("\n");

      long tailleFichier = fichier.length();

      List<WorkerInfo> hosts = Project.getWorkers();

      /* On limite la taille maximale d'un fragment pour pas exploser la ram */
      /* le "*4" permet d'utiliser au mieux les 4 threads (en moyenne) des pcs de l'école */
      int tailleSegment = Integer.min((int) (tailleFichier / (hosts.size() * 4)), 500000000);

      int listeTaille = hosts.size();
      while (!eof) {
          sb.delete(0, sb.length());

          while (sb.length() <= tailleSegment && !eof) {
            String lu = br.readLine();
            if (lu == null)
              eof = true;
            else {
              sb.append(lu);
              sb.append(finLigne);
            }
          }

          limitThreads.acquire();

          if (problemeRencontre.get()) {
            System.err.println("Problème rencontré sur un des Envoyeurs. Arrêt");
            /* On oublie pas de release pour ne pas fausser l'attente de tous
             * les threads */
            limitThreads.release();
            break;
          }

          String s = sb.toString();
          WorkerInfo host = hosts.get(id % listeTaille);
          new Thread(new HdfsEnvoyeur(limitThreads, problemeRencontre,
                host.hostname, host.portHdfs, fname, id, s)).start();
          id++;
      }

      br.close();
    }
    catch(Exception e) {
      e.printStackTrace();
      return;
    }

    System.out.println("Attente de tous les Threads");

    try {
      for (int i = 0; i < NB_THREADS; i++)
        limitThreads.acquire();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }

  public static void HdfsRead(String fname) {
    /* Limite le nombre de threads en même temps (éviter d'exploser la ram du client) */
    Semaphore limitThreads = new Semaphore(NB_THREADS);

    /* On récupère la liste des fragments */
    ConcurrentMap<Integer, WorkerInfo> listeFragments = getHostsFragments(fname);

    /* Les morceaux reçus par numéro de fragment */
    ConcurrentMap<Integer, String> resultats = new ConcurrentHashMap<>();

    AtomicBoolean problemeRencontre = new AtomicBoolean(false);

    /* On récupère le numéro de fragment max (== nombre de fragments - 1) */
    int nbFrags = 0; /* le nombre de fragments */
    for (int idReceveur : listeFragments.keySet())
      nbFrags = Integer.max(nbFrags, idReceveur + 1);

    BufferedWriter bw;
    try {
      File fichier = new File(fname + "_read");
      bw = new BufferedWriter(new FileWriter(fichier));
      try {
        int idReceveur = 0;
        int idDernierEcrit = 0;

        while (idReceveur < nbFrags) {
          limitThreads.acquire();

          if (resultats.containsKey(idDernierEcrit)) {
            bw.append(resultats.get(idDernierEcrit));
            resultats.remove(idDernierEcrit);
            idDernierEcrit++;
          }

          if (problemeRencontre.get()) {
            System.err.println("Problème rencontré sur un des Receveurs. Arrêt");
            /* On oublie pas de release pour ne pas fausser l'attente de tous
             * les threads */
            limitThreads.release();
            break;
          }

          WorkerInfo nextHost = listeFragments.get(idReceveur);
          if (nextHost == null)
            throw new RuntimeException("Le fragment numéro " + idReceveur + " n'existe sur aucun serveur");

          new Thread(new HdfsReceveur(limitThreads, problemeRencontre,
                nextHost.hostname, nextHost.portHdfs, fname, idReceveur, resultats)).start();

          idReceveur++;
        }

        System.out.println("Attente de tous les Threads");

        for (int i = 0; i < NB_THREADS; i++) {
          limitThreads.acquire();
          if (resultats.containsKey(idDernierEcrit)) {
            bw.append(resultats.get(idDernierEcrit));
            resultats.remove(idDernierEcrit);
            idDernierEcrit++;
          }
        }

        while (idDernierEcrit < nbFrags) {
          if (resultats.containsKey(idDernierEcrit)) {
            bw.append(resultats.get(idDernierEcrit));
            resultats.remove(idDernierEcrit);
            idDernierEcrit++;
          }
        }

      }
      finally {
        bw.close();
      }
    }
    catch(Exception e) {
      e.printStackTrace();
      return;
    }
  }


  //    private static void envoyer(String args) {
  //      Semaphore limitThreads = new Semaphore(NB_THREADS);
  //  
  //      AtomicBoolean problemeRencontre = new AtomicBoolean(false);
  //  
  //      try {
  //        File fichier = new File(args);
  //        BufferedReader br = new BufferedReader(new FileReader(fichier));
  //        boolean eof = false;
  //        int id = 0;
  //  
  //        StringBuilder sb = new StringBuilder();
  //        StringBuilder finLigne = new StringBuilder("\n");
  //        int tailleSegment = 100000000;
  //  
  //        while (!eof) {
  //  
  //          try {
  //            sb.delete(0, sb.length());
  //  
  //            while (sb.length() <= tailleSegment && !eof) {
  //              String lu = br.readLine();
  //              if (lu == null)
  //                eof = true;
  //              else {
  //                sb.append(lu);
  //                sb.append(finLigne);
  //              }
  //            }
  //  
  //            limitThreads.acquire();
  //  
  //            if (problemeRencontre.get()) {
  //              System.err.println("Problème rencontré sur un des Envoyeurs. Arrêt");
  //              /* On oublie pas de release pour ne pas fausser l'attente de tous
  //               * les threads */
  //              limitThreads.release();
  //              break;
  //            }
  //  
  //            String s = sb.toString();
  //            new Thread(new HdfsEnvoyeur(limitThreads, problemeRencontre,
  //                  "carpe.enseeiht.fr", 6666, args, id, s)).start();
  //            id++;
  //          } catch(OutOfMemoryError oom) {
  //            System.out.println("out of memory !!");
  //            TimeUnit.SECONDS.sleep(10);
  //          }
  //        }
  //  
  //        br.close();
  //      }
  //      catch(Exception e) {
  //        e.printStackTrace();
  //        return;
  //      }
  //  
  //      System.out.println("Attente de tous les Threads");
  //  
  //      try {
  //        for (int i = 0; i < NB_THREADS; i++)
  //          limitThreads.acquire();
  //      }
  //      catch(Exception e) {
  //        e.printStackTrace();
  //      }
  //    }


  public static void main(String[] args) {

    if (args.length == 2 && args[0].equals("read"))
      HdfsRead(args[1]);
    else if (args.length == 2 && args[0].equals("delete"))
      HdfsDelete(args[1]);
    else if (args.length == 3 && args[0].equals("write"))
      HdfsWrite(-1, Project.PATH + "data/" + args[2]); /* je met -1 car j'utilise pas le format */
    else
      usage();

    // java HdfsClient <read|write> <txt|kv> <file>
    // appel des méthodes précédentes depuis la ligne de commande

    // /* stocker les résultats de tous les envois
    //  * pour vérifier que tout s'est bien passé */

    // if (args.length > 0) {
    //   envoyer(args[0]);
    //   new Thread(new HdfsChercheur(new Semaphore(1), new AtomicBoolean(false),
    //         "carpe.enseeiht.fr", 6666, args[0], 0, null)).start();
    //   // return;
    // }

    // Semaphore limitThreads = new Semaphore(NB_THREADS);

    // AtomicBoolean problemeRencontre = new AtomicBoolean(false);

    // ConcurrentMap<Integer, String> resultats = new ConcurrentHashMap<>();

    // try {
    //   File fichier = new File(args[0] + "_read");
    //   BufferedWriter bw = new BufferedWriter(new FileWriter(fichier));
    //   int idReceveur = 0;
    //   int idDernierEcrit = 0;

    //   try {
    //     while (idReceveur <= 12) {
    //       limitThreads.acquire();

    //       if (resultats.containsKey(idDernierEcrit)) {
    //         bw.append(resultats.get(idDernierEcrit));
    //         resultats.remove(idDernierEcrit);
    //         idDernierEcrit++;
    //       }

    //       if (problemeRencontre.get()) {
    //         System.err.println("Problème rencontré sur un des Receveurs. Arrêt");
    //         /* On oublie pas de release pour ne pas fausser l'attente de tous
    //          * les threads */
    //         limitThreads.release();
    //         break;
    //       }

    //       new Thread(new HdfsReceveur(limitThreads, problemeRencontre,
    //             "carpe.enseeiht.fr", 6666, args[0], idReceveur, resultats)).start();

    //       idReceveur++;
    //     }

    //     System.out.println("Attente de tous les Threads");

    //     for (int i = 0; i < NB_THREADS; i++) {
    //       limitThreads.acquire();
    //       if (resultats.containsKey(idDernierEcrit)) {
    //         bw.append(resultats.get(idDernierEcrit));
    //         resultats.remove(idDernierEcrit);
    //         idDernierEcrit++;
    //       }
    //     }

    //     if (resultats.containsKey(idDernierEcrit)) {
    //       bw.append(resultats.get(idDernierEcrit));
    //       resultats.remove(idDernierEcrit);
    //       idDernierEcrit++;
    //     }
    //     System.out.println(idDernierEcrit);

    //   } catch(OutOfMemoryError oom) {
    //     System.out.println("out of memory !!");
    //     TimeUnit.SECONDS.sleep(10);
    //   }

    //   bw.close();
    // }
    // catch(Exception e) {
    //   e.printStackTrace();
    //   return;
    // }

    System.out.println("main thread exited");
  }


  /** Récupérer le nombre de fragment du fichier et sur quel host chaque fragment est stocké
   * @param fileName le nom du fichier (sans le _partX)
   * @return la map qui associe le numéro d'un fragment à son host
   */
  public static ConcurrentMap<Integer, WorkerInfo> getHostsFragments(String fileName) {
    ConcurrentMap<Integer, WorkerInfo> resultats = new ConcurrentHashMap<>();
    AtomicBoolean problemeRencontre = new AtomicBoolean(false);

    System.out.println("Contact des serveurs pour connaître l'emplacement des fragments");

    Semaphore limitThreads = new Semaphore(NB_THREADS);

    /* hack sale, utiliser une pool de threads */
    try {
      for (WorkerInfo host : Project.getWorkers()) {
        new Thread(new HdfsChercheur(limitThreads, problemeRencontre,
              host, fileName, resultats)).start();
        limitThreads.acquire();
      }

      for (int i = 0; i < NB_THREADS; i++)
        limitThreads.acquire();
    } catch (Exception e) { e.printStackTrace(); }

    if (problemeRencontre.get())
      throw new RuntimeException("Une erreur a été rencontré lors du contact des serveurs, abandon");

    return resultats;
  }
}
