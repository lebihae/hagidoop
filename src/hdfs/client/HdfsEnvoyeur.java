package hdfs.client;

import java.net.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.io.*;

import hdfs.messages.*;

/** Un sender Hdfs qui envoie une partie du fichier au serveur
 * qui lui a été attribué. */
public class HdfsEnvoyeur implements Runnable {

  /** Limiter le nombre de threads actuellement lancés
   * (surtout pour éviter une trop grosse utilisation de RAM) */
  private Semaphore limitThreads;

  /** Si on rencontre une erreur, on met problemeRencontre a true
   * pour indiquer qu'il y a eu un soucis */
  private AtomicBoolean problemeRencontre;

  /** La socket pour l'envoi et la réception d'objets */
  private Socket sock;

  /** Le chemin du fichier d'entrée sur le client */
  private String cheminFichier;

  /** Le numéro de l'envoyeur courant
   * (pour savoir quelle portion de fichier on doit envoyer) */
  private int idEnvoyeur;

  /** Le contenu à écrire dans le fichier sur le serveur */
  private String contenu;

  /** Nom de la machine avec le serveur hdfs */
  private String hostname;

  /** Port de la machine avec le serveur hdfs */
  private int port;

  /** Créer l'envoyeur (pensé pour être dans un Thread)
   * à partir de toutes les infos nécessaires */
  public HdfsEnvoyeur(Semaphore limitThreads, AtomicBoolean problemeRencontre,
      String hostname, int port,
      String cheminFichier, int idEnvoyeur, String contenu) {
    this.limitThreads = limitThreads;
    this.problemeRencontre = problemeRencontre;
    this.hostname = hostname;
    this.port = port;
    this.cheminFichier = cheminFichier;
    this.idEnvoyeur = idEnvoyeur;
    this.contenu = contenu;
  }

  @Override
  public void run() {
    try {
      this.sock = new Socket(hostname, port);
      String chemin_split[] = cheminFichier.split("/");
      String nomFichier =
        cheminFichier.split("/")[chemin_split.length - 1] + "_part" + idEnvoyeur;

      OutputStream os = sock.getOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(os);
      oos.writeObject(new HdfsRequeteEcriture(nomFichier, contenu));

      InputStream is = sock.getInputStream();
      ObjectInputStream ois = new ObjectInputStream(is);
      ((HdfsStatus) ois.readObject()).verifierOK();
      System.out.println(nomFichier + " créé avec succès !");

      ois.close();
      oos.close();
      is.close();
      os.close();
      sock.close();
    }
    catch(Exception e) {
      e.printStackTrace();
      problemeRencontre.set(true);
    }
    limitThreads.release();
  }

}
