package hdfs.utilitaires;

import java.io.*;

/** Représente un fichier sur le disque dur */
public class Fichier {

  /** Chemin du fichier sur le disque dur */
  private String chemin;

  /** Créer un fichier à partir de son nom */
  public Fichier(String chemin) {
    this.chemin = chemin;
  }

  /** Remplacer le contenu du Fichier
   * @param contenu le nouveau contenu du fichier */
  public void ecrire(String contenu)
      throws FileNotFoundException, UnsupportedEncodingException {
    PrintWriter writer = new PrintWriter(chemin, "UTF-8");
    writer.println(contenu);
    writer.close();
  }

}
