package hdfs.messages;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;

/** Un message pour indiquer si l'opération demandée
 * a réussi ou échoué */
public class HdfsStatus implements Serializable {

  /** null si pas d'erreur ou exception */
  private Exception status = null;

  /** la réponse du serveur */
  private byte reponse[] = null;

  /** Constructeur quand on a pas d'erreur ni de message */
  public HdfsStatus() {}

  /** Constructeur quand on a une réponse à envoyer */
  public HdfsStatus(String reponse) {
    /* On compresse la string pour accelerer l'envoi sur le réseau */
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    DeflaterOutputStream defl = new DeflaterOutputStream(out);
    try {
      defl.write(reponse.getBytes());
      defl.flush();
      defl.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    this.reponse = out.toByteArray();
  }

  /** Constructeur quand l'opération a fail */
  public HdfsStatus(Exception erreur) {
    this.status = erreur;
  }

  /** Récupérer le status de la dernière opération du serveur
   * @return l'exception rencontrée ou null si pas d'erreur */
  public Exception getStatus() {
    return status;
  }

  /** Vérifie que l'opération s'est bien passé.
   * Si tout va bien, ne fait rien. Sinon, lance l'exception
   * récupérée depuis le serveur */
  public void verifierOK() throws Exception {
    if (status != null) {
      throw status;
    }
  }

  /** Récupérer la réponse du serveur
   * (dans le cas d'une lecture)
   * @return la réponse du serveur */
  public String getReponse() {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    InflaterOutputStream infl = new InflaterOutputStream(out);
    try {
      infl.write(reponse);
      infl.flush();
      infl.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return new String(out.toByteArray());
  }

  @Override
  public String toString() {
    if (status == null)
      return "Status(OK)";
    else
      return "Status(Erreur, " + status.toString() + ")";
  }
}
