package hdfs.messages;

/** Une exception quand le client demande d'écrire un nom de fichier interdit
 * ex : présence de '/' */
public class NomFichierInterditException extends RuntimeException {

  /** Créer une exception par le message d'erreur */
  public NomFichierInterditException(String message) {
    super(message);
  }

}
