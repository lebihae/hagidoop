package hdfs.messages;

import java.io.*;
import java.util.regex.Pattern;

/** Requete de suppression de fichier au serveur Hdfs */
public class HdfsRequeteSuppression extends HdfsRequete {

  /** Nouvelle requete de suppression de fichier
   * @param nomFichier le nom du fichier à supprimer */
  public HdfsRequeteSuppression(String nomFichier) {
    super(nomFichier);
  }

  public String getOperationNom() {
    return "Suppression";
  }

  public HdfsStatus executerOperation(String chemin) {
    System.out.println("suppression de " + chemin);

    File fichier = new File(chemin);
    File dossier = fichier.getParentFile();
    File[] contenuDossier = dossier.listFiles();
    Pattern fragmentFichier = Pattern.compile(fichier.getName() + "_part[0-9]+");

    for(File f : contenuDossier){
      if (fragmentFichier.matcher(f.getName()).find()) {
        f.delete();
      }
    }

    return new HdfsStatus();
  }

}
