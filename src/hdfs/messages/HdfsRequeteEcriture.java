package hdfs.messages;

import java.io.*;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;

/** Requete d'écriture d'un fichier sur le serveur Hdfs */
public class HdfsRequeteEcriture extends HdfsRequete {

  /** Le nouveau contenu du fichier (compressé) */
  private byte contenu[];

  /** Nouvelle requete de suppression de fichier
   * @param nomFichier le nom du fichier à modifier / créer
   * @param contenu le nouveau contenu du fichier */
  public HdfsRequeteEcriture(String nomFichier, String contenu) {
    super(nomFichier);
    /* On compresse le contenu pour que l'envoi soit plus rapide */
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    DeflaterOutputStream defl = new DeflaterOutputStream(out);
    try {
      defl.write(contenu.getBytes());
      defl.flush();
      defl.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    this.contenu = out.toByteArray();
  }

  public String getOperationNom() {
    return "Ecrire(" + contenu + ")";
  }

  /** Décompresser le contenu pour retrouver la String d'origine */
  private String getContenu() {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    InflaterOutputStream infl = new InflaterOutputStream(out);
    try {
      infl.write(contenu);
      infl.flush();
      infl.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return new String(out.toByteArray());
  }

  @Override
  public HdfsStatus executerOperation(String chemin) {

    File fichier = new File(chemin);
    try {
      BufferedWriter bw = new BufferedWriter(new FileWriter(fichier));
      bw.write(getContenu());
      bw.close();
    } catch (Exception e) {
      return new HdfsStatus(e);
    }
    return new HdfsStatus();
  }

}
