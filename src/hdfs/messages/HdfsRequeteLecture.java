package hdfs.messages;

import java.io.*;

/** Requete de lecture d'un fichier depuis le serveur Hdfs */
public class HdfsRequeteLecture extends HdfsRequete {

  /** Nouvelle requete de suppression de fichier
   * @param nomFichier le nom du fichier à lire */
  public HdfsRequeteLecture(String nomFichier) {
    super(nomFichier);
  }

  public String getOperationNom() {
    return "Lecture";
  }

  public HdfsStatus executerOperation(String chemin) {
    try {
      File fichier = new File(chemin);
      BufferedReader br = new BufferedReader(new FileReader(fichier));

      StringBuilder sb = new StringBuilder();
      StringBuilder finLigne = new StringBuilder("\n");

      String lu;
      do {
        lu = br.readLine();

        if (lu != null) {
          sb.append(lu);
          sb.append(finLigne);
        }
      } while (lu != null);

      br.close();
      return new HdfsStatus(sb.toString());
    }
    catch (Exception e) {
      e.printStackTrace();
      return new HdfsStatus(e);
    }
  }

}
