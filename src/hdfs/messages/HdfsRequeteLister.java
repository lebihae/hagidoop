package hdfs.messages;

import java.io.*;
import java.util.regex.Pattern;

/** Lister les fragments d'un fichier existant sur le serveur
 * But: retourner une liste fichier_parti/fichier_partj/… pour
 *      savoir quels fragments du fichier doit-on demander à
 *      serveur hdfs lors de la requete de lecture */
public class HdfsRequeteLister extends HdfsRequete {

  /** Nouvelle requete de suppression de fichier
   * @param nomFichier le nom du fichier à lire */
  public HdfsRequeteLister(String nomFichier) {
    super(nomFichier);
  }

  public String getOperationNom() {
    return "Lister";
  }

  public HdfsStatus executerOperation(String chemin) {
    File fichier = new File(chemin);
    File dossier = fichier.getParentFile();
    File[] contenuDossier = dossier.listFiles();
    StringBuilder sb = new StringBuilder();
    Pattern fragmentFichier = Pattern.compile(fichier.getName() + "_part[0-9]+");

    for(File f : contenuDossier){
      if (fragmentFichier.matcher(f.getName()).find()) {
        if (sb.length() > 0)
          sb.append("/");
        sb.append(f.getName());
      }
    }

    return new HdfsStatus(sb.toString());
  }

}
