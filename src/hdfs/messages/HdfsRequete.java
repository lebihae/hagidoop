package hdfs.messages;
import java.io.*;

/** Interface d'une requete au serveur Hdfs */
abstract public class HdfsRequete implements Serializable {

  /** Le nom du fichier */
  private String nomFichier;

  /** Créer à partir du nom du fichier sur lequel
   * appliquer une opération */
  public HdfsRequete(String nomFichier) {
    this.nomFichier = nomFichier;
  }

  /** Le nom de fichier sur lequel le serveur doit
   * exécuter l'opération
   * @return le nom de fichier */
  public String getNomFichier() {
    return nomFichier;
  };

  /** Code de l'opération qui sera exécutée
   * @param chemin le chemin vers le fichier sur lequel appliquer
   * l'opération sur le serveur
   * @return le status de l'opération */
  abstract protected HdfsStatus executerOperation(String chemin);

  /** Exécuter l'opération sur le serveur
   * @param dossier le dossier dans lequel se trouve le fichier
   * sur le serveur
   * @return le status de l'opération */
  public HdfsStatus executer(String dossier) {
    /* On vérifie dans cette fonction pour être sûr que la
     * vérification soit bien faite sur le serveur */
    if (nomFichier.indexOf('/') >= 0) {
      return new HdfsStatus(new NomFichierInterditException(
          "Caractère '/' interdit dans le nom du fichier"));
    }
    return executerOperation(dossier + "/" + nomFichier);
  }

  /** Récuperer le nom de l'opération */
  abstract protected String getOperationNom();

  @Override
  public String toString() {
    return "Requête(" + getOperationNom() + ", " + nomFichier + ")";
  }
}
