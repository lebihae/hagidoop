package interfaces;

public interface NetworkReaderWriter extends ReaderWriter {
  /** Ouverture de la socket côté serveur (exécuté par Worker) */
	public void openServer();

  /** Ouverture de la socket côté client (exécuté par MapControl) */
	public void openClient();

  /** Inutile mais dans l'interface d'origine */
	public NetworkReaderWriter accept();

  /** Fermer la socket côté serveur (permet aussi d'envoyer la data manquante) */
	public void closeServer();

  /** Fermer la socket côté client */
	public void closeClient();
}
