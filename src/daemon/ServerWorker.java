package daemon;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

import interfaces.FileReaderWriter;
import interfaces.Map;
import interfaces.NetworkReaderWriter;

import java.util.concurrent.*;

public class ServerWorker extends UnicastRemoteObject implements Worker {

  /** La liste des connexions par socket ouvertes par leur ID
   * (permet au writer d'envoyer les résultats au client) */
  public static ConcurrentMap<Integer, Socket> connexions = new ConcurrentHashMap<>();

  /** Le chemin où le hdfs et le worker conservent les fichiers */
  public static String path;

  private static int IDConnexion = 0;

  public ServerWorker() throws RemoteException {
    /* On redéfinit le constructeur pour lancer l'exception vérifiée */
  }

  @Override
  public void runMap (Map m, FileReaderWriter reader, NetworkReaderWriter writer) throws RemoteException {
    /* Ouvrir la socket d'envoi (on récupère dans le dictionnaire "connexions") */
    writer.openServer();

    /* Ouvrir le fichier en lecture */
    reader.open("r");

    /* Exécuter le map */
    m.map(reader, writer);

    /* Fermer le fichier */
    reader.close();

    /* Envoyer la data et fermer la connexion */
    writer.closeServer();

    /* indiquer au garbage collector qu'il peut nettoyer la ram
     * c'est utile ici uniquement car on peut transferer des objets très gros
     * et qu'il est inutile de les conserver jusqu'à la prochaine connexion */
    System.gc();
  }

  public static void main(String[] args) {
    int portRMI = Integer.parseInt(args[0]);
    int portSocket = Integer.parseInt(args[1]);

    path = args[2];

    try {
      /* créer le serveur RMI */
      Worker worker = new ServerWorker();
      LocateRegistry.createRegistry(portRMI);
      Naming.rebind("//localhost:" + portRMI + "/worker", worker);
    } catch(IOException e) {
      e.printStackTrace();
    }

    /* créer le serveur de socket */
    try {
      ServerSocket ssock = new ServerSocket(portSocket);

      try {
        System.out.println("Serveur de socket prêt");

        while (true) {
          Socket s = ssock.accept();
          System.out.println("Connexion d'un client");

          /* On enregistre la socket et on envoie l'id de connexion au client
           * (ça sera utilisé par le writer envoyé à runMap) */
          connexions.put(IDConnexion, s);
          OutputStream os = s.getOutputStream();
          (new DataOutputStream(os)).writeInt(IDConnexion);

          /* On incrémente l'id pour le suivant */
          IDConnexion++;
        }
      } catch(IOException e) {
        e.printStackTrace();
      }

      ssock.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }
}
