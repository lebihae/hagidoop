package daemon;

import java.rmi.Remote;
import java.rmi.RemoteException;

import interfaces.FileReaderWriter;
import interfaces.Map;
import interfaces.NetworkReaderWriter;

public interface Worker extends Remote {
  /** Exécuter l'opération de map sur le noeud courant
   * @param m l'opération de map
   * @param reader l'objet pour lire le fichier (texte ou de KV)
   * @param writer l'objet pour écrire les KVs générés (et les envoyer)
   */
	public void runMap (Map m, FileReaderWriter reader, NetworkReaderWriter writer) throws RemoteException;
}
