package config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Project {
  public static String PATH = "/home/pisento/Documents/hagidoop/";

  /** Récupérer la liste de tous les workers depuis le fichier de config PATH+"/hosts.txt"
   * @return la liste des informations sur les workers
   */
  public static List<WorkerInfo> getWorkers() {
    List<WorkerInfo> listWorkers = new LinkedList<>();

    try
    {
      /* Le fichier d'entrée */
      File file = new File(PATH + "hosts.txt");    
      /* Créer l'objet File Reader */
      FileReader fr = new FileReader(file);  
      /* Créer l'objet BufferedReader */
      BufferedReader br = new BufferedReader(fr);  
      String line;
      while((line = br.readLine()) != null)
      {
        String host[] = line.split(" ");
        try {
          int portRmi = Integer.parseInt(host[1]);
          int portWorkerSocket = Integer.parseInt(host[2]);
          int portHdfs = Integer.parseInt(host[3]);
          listWorkers.add(new WorkerInfo(host[0], portRmi, portWorkerSocket, portHdfs));
        } catch (NumberFormatException e) {
          System.out.println("L'un des ports du host '" + line + "' est invalide. La ligne sera ignorée");
        } catch (ArrayIndexOutOfBoundsException e) {
          System.out.println("Il manque des informations sur le host '" + line + "'. La ligne sera ignorée");
        }
      }
      fr.close();    
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }

    return listWorkers;
  }
}
