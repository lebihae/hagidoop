package config;

/**
 * WorkerInfo: permet d'avoir les informations (adresse, portRMI, portSocket)
 * d'un ServerWorker
 */
public class WorkerInfo {

  /** L'adresse du worker */
  public final String hostname;

  /** Le port pour exécuter le map en RMI */
  public final int portRMI;

  /** Le port pour recevoir les résultats via une socket */
  public final int portSocket;

  /** Le port de la socket hdfs (si le serveur hdfs est lancé) sinon -1 */
  public final int portHdfs;

  /** Définir un nouveau worker par ses informations
   * @param hostname son adresse
   * @param portRMI son port pour le RMI
   * @param portSocket son port pour l'envoi avec une socket
   */
  public WorkerInfo(String hostname, int portRMI, int portSocket) {
    this.hostname = hostname;
    this.portRMI = portRMI;
    this.portSocket = portSocket;
    this.portHdfs = 8000; /* port hdfs par défaut */
  }
  
  /** Définir un nouveau worker par ses informations
   * @param hostname son adresse
   * @param portRMI son port pour le RMI
   * @param portSocket son port pour l'envoi avec une socket
   * @param portHdfs son port pour le serveur hdfs (si lancé)
   */
  public WorkerInfo(String hostname, int portRMI, int portSocket, int portHdfs) {
    this.hostname = hostname;
    this.portRMI = portRMI;
    this.portSocket = portSocket;
    this.portHdfs = portHdfs;
  }
}
