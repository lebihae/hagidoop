package io;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

import interfaces.KV;
import interfaces.ReaderWriter;

/**
 * La fifo pour que le réduceur récupère tous les résultats
 * de chaque opération Map
 */
public class QueueResults implements ReaderWriter {

  /** Garder les KVs en mémoire pour qu'on puisse les lire */
  private Queue<KV> fifo = new ConcurrentLinkedQueue<>();

  /** Permet au thread de s'endormir jusqu'à ce qu'une valeur puisse être lue */
  private Semaphore valueAvailable = new Semaphore(0);

  @Override
  public void write(KV record) {
    /* On peut pas insérer "null" dans la fifo
     * donc on met un KV avec la clé à "null"
     * pour indiquer la fin du programme */
    if (record == null)
      fifo.offer(new KV(null, null));
    else
      fifo.offer(record);
    valueAvailable.release();
  }

  @Override
  public KV read() {
    try {
      valueAvailable.acquire();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    KV kv = fifo.poll();

    /* On indique la fin de tous les threads
     * avec un KV dont la clé est "null" */
    if (kv.k == null)
      return null;
    else
      return kv;
  }

}
