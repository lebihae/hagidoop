package io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import interfaces.KV;
import interfaces.Reader;

public class ReaderKV implements Reader {
  private BufferedReader reader;

  public ReaderKV(FileReader file) {
    this.reader = new BufferedReader(file);
  }

  private String readWord(BufferedReader reader) throws IOException {
    String word = "";
    int character;
    while ((character = reader.read()) != -1) {
      if (Character.isWhitespace(character)) {
        break;
      }
      word += (char) character;
    }
    return word;
  }

  @Override
  public KV read() {
    try {
      try {
        String word = readWord(this.reader);
        String[] parts = word.split(KV.SEPARATOR);
        if (parts.length != 2) {
          throw new IOException("Malformed word :" + word + ", expecting <key>" + KV.SEPARATOR + "<value>");
        }
        return new KV(parts[0], parts[1]);
      } finally {
        reader.close();
      }
    } catch(IOException e) {
      e.printStackTrace();
    }
    /* signifie qu'on a fini ou qu'il y a eu une erreur */
    return null;
  }
}
