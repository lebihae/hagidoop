package io;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Queue;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;
import java.util.LinkedList;

import daemon.ServerWorker;
import interfaces.*;

/** Envoyer et recevoir les KVs obtenus après le map sur le Worker par le réseau */
public class NetworkReaderWriterKV implements NetworkReaderWriter {

  /** L'ID de la connexion pour pouvoir récupérer
   * la socket côté serveur */
  private int IDConnexion;

  /** le stream pour recevoir les KVs (côté client) */
  private transient InputStream is; /* Le mot clé transient force à ne pas sérialiser l'attribut */

  /** le stream pour envoyer les KVs (côté serveur) */
  private transient OutputStream os;

  /** la socket pour se connecter au Worker */
  private transient Socket clientSocket;

  /** Garder les KVs en mémoire avant de les envoyer */
  private Queue<KV> buffer = new LinkedList<>();

  /** Créer un NetworkReaderWriterKV en initialisant la connexion avec un Worker
   * @param hostname l'addresse du serveur
   * @param port le port du serveur de socket
   */
  public NetworkReaderWriterKV(String hostname, int port) throws IOException {
    clientSocket = new Socket(hostname, port);
    is = clientSocket.getInputStream();

    /* On récupère l'id de la connexion */
    IDConnexion = (new DataInputStream(is)).readInt();
    System.out.println("connexion id =" + IDConnexion);
  }

  @Override
  public void openServer() {
    try {
      /* On récupère l'OutputStream depuis le Worker grâce à notre ID */
      os = ServerWorker.connexions.get(IDConnexion).getOutputStream();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void openClient() {
    /* ne rien faire car l'input stream
     * a déjà été initialisé par le constructeur */
  }

  @Override
  public NetworkReaderWriter accept() {
    /* ne rien faire car non utilisée */
    return null;
  }

  /** Compresser et envoyer tout le contenu du buffer */
  private void envoyerData() throws IOException {
    /* compression du buffer */
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    DeflaterOutputStream defl = new DeflaterOutputStream(out);

    /* on append tous les KVs à la suite séparés
     * par le symbole séparateur */
    for (KV kv : buffer) {
      /* on envoie: clé0<->valeur0<->clé1<->valeur1<->… */
      defl.write(kv.k.toString().getBytes());
      defl.write(KV.SEPARATOR.getBytes());
      defl.write(kv.v.toString().getBytes());
      defl.write(KV.SEPARATOR.getBytes());
    }
    /* le séparateur est doublé pour marquer la fin de la transmission */
    defl.write(KV.SEPARATOR.getBytes());
    defl.flush();
    defl.close();

    /* on envoie les données compressées et on vide le buffer */
    os.write(out.toByteArray());
    buffer.clear();
  }

  @Override
  public void closeServer() {
    if (os != null) {
      try {
        /* On envoie tout ce qui manque */
        envoyerData();

        os.close();
        ServerWorker.connexions.get(IDConnexion).close();
        /* On retire de la liste des connexions ouvertes */
        ServerWorker.connexions.remove(IDConnexion);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void closeClient() {
    /* On ferme la connexion définitivement */
    try {
      if (is != null)
        is.close();
      is = null;
      if (clientSocket != null)
        clientSocket.close();
      clientSocket = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void write(KV record) {
    /* garder en mémoire les KVs,
     * on les enverra tous d'un coup à la fermeture de la connexion par le serveur
     * (ça nous permettra de compresser les données) */
    buffer.offer(record);

    /* Optimisation future: envoyer dès qu'on atteint un certain seuil les données déjà en buffer
     * pour éviter de tout envoyer en une seule fois mais quand même pouvoir faire de la compression */
  }

  /** Recevoir toutes les données présentes sur la socket et
   * retrouver les KVs (que l'on stocke dans le buffer) */
  private void recevoirKVs() throws IOException {
    /* on décompresse les données */
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    InflaterOutputStream infl = new InflaterOutputStream(out);

    infl.write(is.readAllBytes()); /* on lit toutes les données de la socket */
    infl.flush();
    infl.close();

    /* on récupère le message décompressé */
    String message = new String(out.toByteArray());

    String tempKey = null;
    for (String s: message.split(KV.SEPARATOR)) {
      if (tempKey == null) {
        tempKey = s;
      }
      else {
        /* on reconstruit le KV courant et on l'ajoute au buffer */
        buffer.offer(new KV(tempKey, s));
        tempKey = null;
      }
    }

    /* Si on a le séparateur 2 fois d'affilée dans le message,
     * alors on ajoute "null" au buffer pour annoncer la fin du map */
    if (message.contains(KV.SEPARATOR + KV.SEPARATOR))
      buffer.offer(null);
  }

  @Override
  public KV read() {
    /* Lire toutes les données depuis la socket et décompresser la data.
     * Puis renvoyer uniquement le premier KV reçu et mettre les autres dans le buffer */
    try {
      if (buffer.isEmpty()) {
        recevoirKVs();
      }
      KV kv = buffer.poll(); /* peut être le kv "null" si fin de transmission */
      return kv;
    } catch(IOException e) {
      e.printStackTrace();
    }
    /* erreur */
    return null;
  }
}
