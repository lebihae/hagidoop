package io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import daemon.ServerWorker;
import interfaces.FileReaderWriter;
import interfaces.KV;
import interfaces.Reader;

public class ReaderText implements FileReaderWriter {

  /** Le lecteur du fichier */
  private BufferedReader reader = null;

  /** L'ecrivain du fichier */
  private BufferedWriter writer = null;

  /** Le nom du fichier à Lire */
  private String fileName;

  public ReaderText(String fileName) {
    this.fileName = fileName;
  }


  @Override
  public void open(String mode) {
    /* On ouvre le fichier si pas encore ouvert */
    try {
      if (mode.contains("r") && reader == null) {
        FileReader file = new FileReader(ServerWorker.path + "/" + fileName);
        this.reader = new BufferedReader(file);
      }
      if (mode.contains("w") && writer == null) {
        FileWriter file = new FileWriter(fileName);
        this.writer = new BufferedWriter(file);
        writer.write("");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void close() {
    try {
      if (reader != null) {
        reader.close();
      }
      if (writer != null) {
        writer.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public String getFname() {
    return fileName;
  }

  @Override
  public void setFname(String fname) {
    // TODO Auto-generated method stub
  }

  @Override
  public long getIndex() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public KV read() {
    try {
      String line = reader.readLine();
      if (line != null)
        return new KV("cle", line);

    } catch (IOException e) {
      e.printStackTrace();
    }

    /* on renvoie donc null pour stopper l'appelant */
    return null;
  }

  @Override
  public void write(KV record) {
    try {
      writer.write(record.k+"<->"+record.v.toString());
      writer.newLine();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
