{ pkgs ? import <nixpkgs> {} }:
let
  my-python-packages = ps: with ps; [
    matplotlib
    networkx
    # other python packages
  ];
  in
  (pkgs.buildFHSEnv {
   name = "python-fhs-env";
   targetPkgs = pkgs: (with pkgs; [
       (python3.withPackages my-python-packages)
   ]);
   runScript = "bash";
   }).env
