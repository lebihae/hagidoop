#!/usr/bin/env python
import matplotlib.pyplot as plt

x = [5, 10, 21, 42, 84, 167]
t = [20, 40, 81, 163, 327, 651]

y = [6.1, 3.3, 2.0, 1.8, 1.4, 2.5]

plt.plot(t, y)

# Set diagram title
plt.suptitle("Temps d'excécution du Map-Reduce en fonction du nombre de threads, sur un ifichier de 4.2 Go")

plt.xlabel("Nombre de threads")
plt.ylabel("Temps d'excécution du Map-Reduce [s] ")

plt.savefig("result.png")

